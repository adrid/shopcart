<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Entity\ShoppingCartLog;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class ShoppingCartController extends Controller
{
    /**
     * @Route("/cart", name="cart")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showCartAction(Request $request)
    {
        return $this->render('AppBundle:ShoppingCart:cart.html.twig',array(
            'cart' => $request->getSession()->has('cart')? $request->getSession()->get('cart'): array()
        ));
    }

    /**
     * @Route("/addToCart/{id}", name="cart_add")
     *
     * @param Request $request
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addToCartAction(Request $request, Product $product) {
        $form = ShoppingCartController::addToCartForm($this, $request, $product);
        $form->handleRequest($request);

        if($form->isValid()) {
            $amount = $form->get('amount')->getData();
            $this->addProductToCart($product, $amount, $request);

            return $this->redirectToRoute('product_show', array(
                'id' => $product->getId()
            ));
        }

        return $this->render('AppBundle:Product:show.html.twig',array(
            'product' => $product,
            'form'=> $form->createView()
        ));
    }

    /**
     * @Route("/emptyCart", name="cart_empty")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function emptyCart(Request $request) {
        $session = $request->getSession();

        if($session->has('cart')) {
            $session->remove('cart');
        }

        return $this->redirectToRoute('product');
    }

    /**
     * @Route("/emptyCart/{id}", name="cart_empty_by_id")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function emptyCartById(Request $request, $id) {
        $session = $request->getSession();

        if($session->has('cart')) {
            $cart = $session->get('cart');
            if(array_key_exists($id, $cart)) {
                unset($cart[$id]);
                $session->set('cart', $cart);
                $session->save();
            }
        }

        return $this->redirectToRoute('product');
    }

    /**
     * @param Controller $controller
     * @param Request $request
     * @param Product $product
     * @return \Symfony\Component\Form\Form
     */
    public static function addToCartForm(Controller $controller, Request $request, Product $product) {
        return $controller->createFormBuilder($product, array(
            'action' => $controller->generateUrl('cart_add', array('id' => $product->getId())),
            'method' => 'POST',
            'validation_groups' => 'ordering'
        ))
            ->add('amount', 'integer', array(
                'mapped' => false,
                'data' => 1,
                'constraints' => array(
                    new NotBlank(array('groups' => array('ordering'))),
                    new Range(array(
                        'groups' => array('ordering'),
                        'max'=> $product->getAmount() - ShoppingCartController::itemsForProductInCart($request, $product),
                        'min'=> 1,
                        'maxMessage' => 'You can\'t buy more products than we have'
                    )),
                )
            ))
            ->add('add', 'submit', array(
                'label' => 'Add to cart',
                'disabled' => !$product->isAvailable()
            ))
            ->getForm();
    }

    private static function itemsForProductInCart(Request $request, Product $product) {
        $cart = ShoppingCartController::getCartFromSession($request);

        if(array_key_exists($product->getId(), $cart)
            && array_key_exists('amount',$cart[$product->getId()])) {
            return $cart[$product->getId()]['amount'];
        }

        return 0;
    }

    private static function getCartFromSession(Request $request) {
        $session = $request->getSession();

        if($session->has('cart')) {
            $cart = $session->get('cart');
        } else {
            $cart = array();
        }

        return $cart;
    }

    private function logProduct(Product $product) {
        $em = $this->getDoctrine()->getManager();
        $log = new ShoppingCartLog($product);
        $em->persist($log);
        $em->flush();
    }

    private function addProductToCart(Product $product, $amount, Request $request) {
        $cart = $this->getCartFromSession($request);

        $session = $request->getSession();
        $cart[$product->getId()]['name'] = $product->getName();
        if(array_key_exists('amount', $cart[$product->getId()])) {
            $cart[$product->getId()]['amount'] += $amount;
        } else {
            $cart[$product->getId()]['amount'] = $amount;
        }

        $session->set('cart', $cart);
        $session->save();

        $this->logProduct($product);
    }

}

