<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Manufacturer;
use AppBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductController
 * @package AppBundle\Controller
 * @Route("/")
 */
class ProductController extends Controller
{
    /**
     * @Route("/", name="product")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository('AppBundle:Product')->findAll();

        return $this->render('@App/Product/index.html.twig', array('products' => $products));
    }

    /**
     * @Route("/manufacturer/{id}", name="product_by_manufacturer")
     * @param Manufacturer $manufacturer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexByManufacturerAction(Manufacturer $manufacturer) {
        $products = $manufacturer->getProducts();
        return $this->render('@App/Product/index.html.twig', array('products' => $products));
    }

    public function listManufacturersAction() {
        $em = $this->getDoctrine()->getManager();
        $manufacturers = $em->getRepository('AppBundle:Manufacturer')->findAll();

        return $this->render('@App/Product/manufacturersList.html.twig', array('manufacturers' => $manufacturers));

    }

    /**
     * @Route("/details/{id}", name="product_show")
     * @ParamConverter("product", class="AppBundle:Product")
     * @param Request $request
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(Request $request, Product $product) {

        $form = ShoppingCartController::addToCartForm($this, $request, $product);
        return $this->render('@App/Product/show.html.twig',
            array(
                'product' => $product,
                'form' => $form->createView()
                ));
    }


}
