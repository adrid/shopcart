<?php


namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Manufacturer;
use AppBundle\Entity\Product;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadProductsData implements FixtureInterface
{
    /**
     * @var ObjectManager
     */
    public $om;

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->om = $manager;

        $manufacturers = array_map(array($this, 'createManufacturer'), $this->manufacturersProvider());
        $products = array_map(array($this, 'createProduct'), $this->productsProvider($manufacturers));

        $manager->flush();
    }

    private function createManufacturer($name) {
        $manufacturer = new Manufacturer($name);
        $this->om->persist($manufacturer);

        return $manufacturer;
    }

    private function createProduct($item) {
        $product = new Product($item['name'], $item['manufacturer'], $item['price'], $item['amount']);
        $product->setDescription($item['description']);
        $this->om->persist($product);

        return $product;
    }

    private function manufacturersProvider() {
        return array(
            'Intel',
            'Apple',
            'IBM',
        );
    }

    private function productsProvider($manufacturers) {
        return array(
            array(
                'name'=> 'Product 1',
                'manufacturer' => $manufacturers[0],
                'price' => '12.00',
                'amount' => 5,
                'description' => null
            ),
            array(
                'name'=> 'Product 2',
                'manufacturer' => $manufacturers[0],
                'price' => '50.00',
                'amount' => 8,
                'description' => 'Description of product 2'
            ),
            array(
                'name'=> 'Product 3',
                'manufacturer' => $manufacturers[1],
                'price' => '14.00',
                'amount' => 2,
                'description' => 'Description of product 3'
            ),
            array(
                'name'=> 'Product 4',
                'manufacturer' => $manufacturers[2],
                'price' => '20.00',
                'amount' => 0,
                'description' => 'Description of product 4. Not available now.'
            ),
            array(
                'name'=> 'Product 5',
                'manufacturer' => $manufacturers[1],
                'price' => '25.00',
                'amount' => 0,
                'description' => 'Description of product 5. Not available now.'
            ),
        );
    }

}